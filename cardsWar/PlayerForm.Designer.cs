﻿namespace cardsWar
{
    partial class PlayerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlayerForm));
            this.LBL_scoreDes = new System.Windows.Forms.Label();
            this.LBL_score = new System.Windows.Forms.Label();
            this.LBL_opponentScore = new System.Windows.Forms.Label();
            this.LBL_oppDesScore = new System.Windows.Forms.Label();
            this.BTN_forfeit = new System.Windows.Forms.Button();
            this.IMG_opponentCard = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.IMG_opponentCard)).BeginInit();
            this.SuspendLayout();
            // 
            // LBL_scoreDes
            // 
            this.LBL_scoreDes.AutoSize = true;
            this.LBL_scoreDes.Location = new System.Drawing.Point(14, 12);
            this.LBL_scoreDes.Name = "LBL_scoreDes";
            this.LBL_scoreDes.Size = new System.Drawing.Size(63, 13);
            this.LBL_scoreDes.TabIndex = 0;
            this.LBL_scoreDes.Text = "Your Score:";
            // 
            // LBL_score
            // 
            this.LBL_score.AutoSize = true;
            this.LBL_score.Location = new System.Drawing.Point(83, 12);
            this.LBL_score.Name = "LBL_score";
            this.LBL_score.Size = new System.Drawing.Size(13, 13);
            this.LBL_score.TabIndex = 1;
            this.LBL_score.Text = "0";
            // 
            // LBL_opponentScore
            // 
            this.LBL_opponentScore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LBL_opponentScore.AutoSize = true;
            this.LBL_opponentScore.Location = new System.Drawing.Point(683, 12);
            this.LBL_opponentScore.Name = "LBL_opponentScore";
            this.LBL_opponentScore.Size = new System.Drawing.Size(13, 13);
            this.LBL_opponentScore.TabIndex = 2;
            this.LBL_opponentScore.Text = "0";
            // 
            // LBL_oppDesScore
            // 
            this.LBL_oppDesScore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LBL_oppDesScore.AutoSize = true;
            this.LBL_oppDesScore.Location = new System.Drawing.Point(582, 12);
            this.LBL_oppDesScore.Name = "LBL_oppDesScore";
            this.LBL_oppDesScore.Size = new System.Drawing.Size(95, 13);
            this.LBL_oppDesScore.TabIndex = 3;
            this.LBL_oppDesScore.Text = "Opponent\'s Score:";
            // 
            // BTN_forfeit
            // 
            this.BTN_forfeit.Location = new System.Drawing.Point(332, 7);
            this.BTN_forfeit.Name = "BTN_forfeit";
            this.BTN_forfeit.Size = new System.Drawing.Size(66, 23);
            this.BTN_forfeit.TabIndex = 5;
            this.BTN_forfeit.Text = "Forfiet";
            this.BTN_forfeit.UseVisualStyleBackColor = true;
            this.BTN_forfeit.Click += new System.EventHandler(this.BTN_forfeit_Click);
            // 
            // IMG_opponentCard
            // 
            this.IMG_opponentCard.Image = global::cardsWar.Properties.Resources.card_back_blue;
            this.IMG_opponentCard.Location = new System.Drawing.Point(315, 36);
            this.IMG_opponentCard.Name = "IMG_opponentCard";
            this.IMG_opponentCard.Size = new System.Drawing.Size(100, 144);
            this.IMG_opponentCard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.IMG_opponentCard.TabIndex = 6;
            this.IMG_opponentCard.TabStop = false;
            // 
            // PlayerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(730, 363);
            this.Controls.Add(this.IMG_opponentCard);
            this.Controls.Add(this.BTN_forfeit);
            this.Controls.Add(this.LBL_oppDesScore);
            this.Controls.Add(this.LBL_opponentScore);
            this.Controls.Add(this.LBL_score);
            this.Controls.Add(this.LBL_scoreDes);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PlayerForm";
            this.Text = "Cards Game";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PlayerForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.IMG_opponentCard)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LBL_scoreDes;
        private System.Windows.Forms.Label LBL_score;
        private System.Windows.Forms.Label LBL_opponentScore;
        private System.Windows.Forms.Label LBL_oppDesScore;
        private System.Windows.Forms.Button BTN_forfeit;
        private System.Windows.Forms.PictureBox IMG_opponentCard;
    }
}

