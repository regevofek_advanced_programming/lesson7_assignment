﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cardsWar
{
    public partial class PlayerForm : Form
    {
        private bool _opponentSelected;
        private bool _playerSelected;
        private int _opponentCard;
        private int _playerCard;
        private string _opponentCardImage;
        private int _myScore;
        private int _opponentScore;
        private bool _gameEnded;
        private PictureBox _previousCard;
        private NetworkStream _clientStream;
        private bool _serverClosed;
        private Thread _client;


        public PlayerForm()
        {
            InitializeComponent();
            _opponentSelected = false;
            _playerSelected = false;
            _myScore = 0;
            _opponentScore = 0;
            _previousCard = null;
            _serverClosed = false;

            _gameEnded = false;
            _client = new Thread(CommunicateWithServer);
            _client.Start();
        }

        private void GenerateCards()
        {
            Random random = new Random();
            int eachCardWidth = (Width - Constants.MARGINS_WIDTH) / (Constants.NUM_OF_CARDS + 1); //x cards with one more "card" spaces (numOfCards + 1)
            int eachCardHeight = Convert.ToInt32(eachCardWidth / Constants.PIC_RATIO);
            int remainingSpaceSize = Width - Constants.MARGINS_WIDTH - (eachCardWidth * Constants.NUM_OF_CARDS); //you have cardsNum + 1 spaces, together one card width
            int nextSpace = remainingSpaceSize / (Constants.NUM_OF_CARDS + 1);
            remainingSpaceSize -= nextSpace;
            int remainingSpaces = Constants.NUM_OF_CARDS;
            Point nextLocation = new Point(nextSpace, Convert.ToInt32(Height - Constants.MARGINS_HEIGHT - (eachCardHeight * (1 + Constants.MARGIN_BOTTOM_PROPORTIONS)))); 
            for (int i = 0; i < Constants.NUM_OF_CARDS; i++)
            {
                PictureBox currentCard = new PictureBox();
                currentCard.Image = Properties.Resources.card_back_red; //set image
                currentCard.Location = nextLocation; //set card location
                currentCard.Size = new Size(eachCardWidth, eachCardHeight); //set the card width and height
                currentCard.SizeMode = PictureBoxSizeMode.StretchImage;

                currentCard.Click += delegate (object sender1, EventArgs e1)
                                        {
                                            IMG_opponentCard.Image = (Image)Properties.Resources.card_back_blue;
                                            if (_previousCard != null)
                                            {
                                                _previousCard.Image = (Image)Properties.Resources.card_back_red;
                                            }
                                            //random the card value
                                            _playerCard = random.Next(1, 13);
                                            //random the card shape
                                            int cardShape = random.Next(1, 4);
                                            
                                            currentCard.Image = (Image)Properties.Resources.ResourceManager.GetObject(cardInfoToString(_playerCard, cardShape, false));

                                            byte[] buffer = new ASCIIEncoding().GetBytes(cardInfoToString(_playerCard, cardShape, true));
                                            _clientStream.Write(buffer, 0, buffer.Length);
                                            _clientStream.Flush();

                                            _playerSelected = true;
                                            if (_opponentSelected)
                                            {
                                                CompareResults();
                                            }
                                            _previousCard = currentCard;
                                        };

                this.Controls.Add(currentCard);
                nextSpace = remainingSpaceSize / remainingSpaces;
                remainingSpaceSize -= nextSpace;
                remainingSpaces--;
                nextLocation.X += eachCardWidth + nextSpace; //one card and a space between cards beginnings
            }
            //this.GenerateOpponentCard(Convert.ToInt32(eachCardWidth * 1.5), Convert.ToInt32(eachCardHeight * 1.5));
        }

        private void CompareResults()
        {
            IMG_opponentCard.Image = (Image)Properties.Resources.ResourceManager.GetObject(_opponentCardImage);
            if (_opponentCard > _playerCard)
            {
                _opponentScore++;
                _myScore--;
            }
            else if (_playerCard > _opponentCard)
            {
                _myScore++;
                _opponentScore--;
            }
            //doing nothing when its the same
            LBL_score.Text = Convert.ToString(_myScore);
            LBL_opponentScore.Text = Convert.ToString(_opponentScore);
            _playerSelected = false;
            _opponentSelected = false;
        }

        private void CommunicateWithServer()
        {
            TcpClient client = new TcpClient();
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse(Constants.SERVER_IP), Constants.SERVER_PORT);

            while (true)
            {
                try
                {
                    client.Connect(serverEndPoint);
                    break;
                }
                catch { }
            }
            _clientStream = client.GetStream();
            _clientStream.ReadTimeout = 100;

            byte[] buffer = new byte[4];
            string input = null;
            while (!_gameEnded)
            {
                try
                {
                    if (_clientStream.Read(buffer, 0, 4) != 0)
                    {
                        input = new ASCIIEncoding().GetString(buffer);
                        switch (input[0])
                        {
                            case Constants.GAME_START_CODE:
                                Invoke((MethodInvoker)GenerateCards);
                                break;
                            case Constants.CARD_MSG_CODE:
                                _opponentCard = Convert.ToInt32(input.Substring(1, 2));
                                int cardShape = 0;
                                switch (input[3])
                                {
                                    case 'C':
                                        cardShape = 1;
                                        break;
                                    case 'D':
                                        cardShape = 2;
                                        break;
                                    case 'H':
                                        cardShape = 3;
                                        break;
                                    case 'S':
                                        cardShape = 4;
                                        break;
                                }
                                _opponentCardImage = cardInfoToString(_opponentCard, cardShape, false);
                                _opponentSelected = true;
                                if (_playerSelected)
                                {
                                    Invoke((MethodInvoker)CompareResults);
                                }
                                break;
                            case Constants.GAME_END_CODE:
                                _gameEnded = true;
                                _serverClosed = true;
                                Invoke((MethodInvoker)Close);
                                break;
                        }
                    }
                }
                catch { }
            }

            _clientStream.Close();
            client.Close();

        }

        private string cardInfoToString(int cardNum, int cardShape, bool forServer)
        {
            string cardString = "";

            if (forServer)
            {
                cardString = Constants.CARD_MSG_CODE.ToString();
                if (cardNum < 10)
                {
                    cardString += '0';
                }
                cardString += cardNum.ToString();
            }
            else
            {
                if (cardNum > 1 && cardNum < 11) //regular card, not an ace or jack, queen, king
                {
                    cardString += "_";
                    cardString += Convert.ToString(cardNum);
                }
                else
                {
                    switch (cardNum)
                    {
                        case 1:
                            cardString += "ace";
                            break;
                        case 11:
                            cardString += "jack";
                            break;
                        case 12:
                            cardString += "queen";
                            break;
                        case 13:
                            cardString += "king";
                            break;
                    }
                }
                cardString += "_of_";
            }
            switch (cardShape)
            {
                case 1:
                    cardString += forServer ? "C" : "clubs";
                    break;
                case 2:
                    cardString += forServer ? "D" : "diamonds";
                    break;
                case 3:
                    cardString += forServer ? "H" : "hearts";
                    break;
                case 4:
                    cardString += forServer ? "S" : "spades";
                    break;
            }
            return cardString;
        }

        
        private void PlayerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            MessageBox.Show("Your score: " + _myScore + " opponent score: " + _opponentScore);
            if (!_serverClosed)
            {
                byte[] buffer = new ASCIIEncoding().GetBytes(Constants.GAME_END_CODE + "000");
                _clientStream.Write(buffer, 0, buffer.Length);
                _clientStream.Flush();
                _gameEnded = true;
                _client.Join();
            }
        }

        private void BTN_forfeit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private static class Constants
        {
            public const int NUM_OF_CARDS = 10;
            public const double PIC_RATIO = 0.703;
            public const int MARGINS_WIDTH = 16;
            public const int MARGINS_HEIGHT = 39;
            public const double MARGIN_BOTTOM_PROPORTIONS = 0.2;
            public const double MARGIN_TOP_PROPORTIONS = 0.3;
            public const char GAME_START_CODE = '0';
            public const char CARD_MSG_CODE = '1';
            public const char GAME_END_CODE = '2';
            public const int SERVER_PORT = 8820;
            public const string SERVER_IP = "127.0.0.1";
        }

    }
}
